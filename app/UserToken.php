<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Airlock\PersonalAccessToken;

/**
 * App\UserToken
 *
 * @property int $id
 * @property string $user_id
 * @property string $name
 * @property string $token
 * @property array|null $abilities
 * @property \Illuminate\Support\Carbon|null $last_used_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $tokenable
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereAbilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereLastUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserToken whereUserId($value)
 * @mixin \Eloquent
 */
class UserToken extends PersonalAccessToken
{
    protected $table = 'users_tokens';

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function tokenable()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
