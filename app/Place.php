<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Place
 *
 * @property int $id
 * @property string $owner_id
 * @property int $place_type_id
 * @property string $name
 * @property string $description
 * @property float $lat
 * @property float $lon
 * @property string $address
 * @property string $postcode
 * @property string $city
 * @property string $country
 * @property string|null $site
 * @property string|null $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place wherePlaceTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Place whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\User $owner
 * @property-read \App\PlaceType $place_type
 * @property-read mixed $avg_rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Review[] $reviews
 * @property-read int|null $reviews_count
 */
class Place extends Model
{
    public $table = 'places';

    protected $with = ['place_type', 'reviews'];
    protected $appends = ['avg_rating'];

    public $fillable = [
        'owner_id', 'place_type_id', 'name', 'description',
        'lat', 'lon', 'address', 'postcode', 'city',
        'country', 'site', 'email', 'phone',
    ];

    protected $hidden = ['owner_id'];

    public static $create_rules = [
        'place_type' => 'required|integer|exists:places_types,id',
        'name' => 'required|string',
        'description' => 'required|string',
        'lat' => 'required|numeric',
        'lon' => 'required|numeric',
        'address' => 'required|string',
        'postcode' => 'required|string',
        'city' => 'required|string',
        'country' => 'required|string',
        'site' => 'nullable|string',
        'email' => 'nullable|email',
        'phone' => 'nullable|string',
    ];

    public static $update_rules = [
        'place_id' => 'integer|exists:places,id',
        'place_type' => 'integer|exists:places_types,id',
        'name' => 'string',
        'description' => 'string',
        'lat' => 'numeric',
        'lon' => 'numeric',
        'address' => 'string',
        'postcode' => 'string',
        'city' => 'string',
        'country' => 'string',
        'site' => 'nullable|string',
        'email' => 'nullable|email',
        'phone' => 'nullable|string',
    ];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    public function place_type()
    {
        return $this->hasOne(PlaceType::class, 'id', 'place_type_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'place_id', 'id');
    }

    public function getAvgRatingAttribute()
    {
        $reviews = $this->reviews()->get();
        $ratingTotal = $reviews->reduce(function ($carry, Review $item) {
            return $carry + $item->rating;
        });

        return ($ratingTotal / $reviews->count());
    }

    public function isOwner(?User $user) {
        $this->attributes['is_owner'] = ($user && $user->id === $this->owner_id);
    }
}
