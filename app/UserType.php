<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\UserType
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserType whereName($value)
 */
class UserType extends Model
{
    public $timestamps = false;
    protected $table = 'users_types';

    protected $fillable = [
        'name',
    ];
}
