<?php

namespace App\Http\Controllers;

use App\Review;
use App\ReviewComment;
use App\User;
use Illuminate\Http\Request;

class ReviewCommentController extends Controller
{

    public function fetch(Request $request)
    {
        $request->validate([
            'review_id' => 'required|integer|exists:reviews,id',
        ]);

        $reviews = ReviewComment::get()->where('review_id', '=', $request->review_id);

        $response = [
            'reviews_comments' => $reviews,
            'reviews_comments_count' => $reviews->count(),
        ];

        return response()->json($response);
    }

    public function create(Request $request)
    {
        $request->validate(ReviewComment::$create_rules);

        /* @var User $user */
        $user = $request->user();

        $review_comment = ReviewComment::create([
            'user_id' => $user->id,
            'review_id' => $request->review,
            'comment' => $request->comment,
        ]);

        return response()->json($review_comment);
    }

    public function update(Request $request)
    {
        $request->validate(ReviewComment::$update_rules);

        /* @var User $user */
        $user = $request->user();

        $review_comment = ReviewComment::find($request->review_comment_id);

        if ($review_comment->user_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        if (isset($request->comment)) $review_comment->comment = $request->comment;

        $review_comment->save();

        return response()->json($review_comment);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'review_comment_id' => 'integer|exists:reviews_comments,id',
        ]);

        /* @var User $user */
        $user = $request->user();

        $review_comment = ReviewComment::find($request->review_comment_id);

        if ($review_comment->user_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        $review_comment->forceDelete();

        return response();
    }

}
