<?php

namespace App\Http\Controllers;

use App\Place;
use App\Review;
use App\ReviewComment;
use App\User;

class DashboardController extends Controller
{
    public function stats()
    {
        return response()->json([
            'users_count' => User::count(),
            'comments_count' => Review::count() + ReviewComment::count(),
            'places_count' => Place::count(),
        ]);
    }
}
