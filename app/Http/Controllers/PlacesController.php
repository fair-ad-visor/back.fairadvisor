<?php

namespace App\Http\Controllers;

use App\User;
use App\Place;
use Illuminate\Http\Request;

class PlacesController extends Controller
{

    public function fetch(Request $request)
    {
        $request->validate([
            'lat_min' => 'required|numeric',
            'lon_min' => 'required|numeric',
            'lat_max' => 'required|numeric',
            'lon_max' => 'required|numeric',
        ]);

        $lat_min = $request->lat_min;
        $lon_min = $request->lon_min;
        $lat_max = $request->lat_max;
        $lon_max = $request->lon_max;

        $places = Place::get()->whereBetween('lat', [$lat_min, $lat_max])->whereBetween('lon', [$lon_min, $lon_max]);

        $response = [
            'places' => $places,
            'places_count' => $places->count(),
        ];

        return response()->json($response);
    }

    public function fetchId(Request $request, Place $place)
    {
        $place->isOwner($request->user());
        return response()->json($place);
    }

    public function create(Request $request)
    {
        $request->validate(Place::$create_rules);

        /* @var User $user */
        $user = $request->user();

        $place = Place::create($new_place_array = [
            'owner_id' => $user->id,
            'place_type_id' => $request->place_type,
            'name' => $request->name,
            'description' => $request->description,
            'lat' => $request->lat,
            'lon' => $request->lon,
            'address' => $request->address,
            'postcode' => $request->postcode,
            'city' => $request->city,
            'country' => $request->country,
            'site' => $request->site,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        $place->isOwner($user);

        return response()->json($place);
    }

    public function update(Request $request)
    {
        $request->validate(Place::$update_rules);

        /* @var User $user */
        $user = $request->user();

        $place = Place::find($request->place_id);

        if ($place->owner_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        if ($request->exists('place_type')) $place->place_type_id = $request->place_type;
        if ($request->exists('name')) $place->name = $request->name;
        if ($request->exists('description')) $place->description = $request->description;
        if ($request->exists('lat')) $place->lat = $request->lat;
        if ($request->exists('lon')) $place->lon = $request->lon;
        if ($request->exists('address')) $place->address = $request->address;
        if ($request->exists('postcode')) $place->postcode = $request->postcode;
        if ($request->exists('city')) $place->city = $request->city;
        if ($request->exists('country')) $place->country = $request->country;
        if ($request->exists('site')) $place->site = $request->site;
        if ($request->exists('email')) $place->email = $request->email;
        if ($request->exists('phone')) $place->phone = $request->phone;

        $place->save();

        $place->isOwner($user);

        return response()->json($place);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'place_id' => 'integer|exists:places,id',
        ]);

        /* @var User $user */
        $user = $request->user();

        $place = Place::findOrNew($request->get('place_id'));

        if ($place->owner_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        $place->forceDelete();

        return response();
    }

}
