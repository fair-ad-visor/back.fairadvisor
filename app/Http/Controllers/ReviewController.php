<?php

namespace App\Http\Controllers;

use App\Review;
use App\User;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

    public function fetch(Request $request)
    {
        $request->validate([
            'place_id' => 'required|integer|exists:places,id',
        ]);

        $reviews = Review::get()->where('place_id', '=', $request->place_id);

        $response = [
            'reviews' => $reviews,
            'reviews_count' => $reviews->count(),
        ];

        return response($response, 200)->header('Content-Type', 'application/json');
    }

    public function create(Request $request)
    {
        $request->validate(Review::$create_rules);

        /* @var User $user */
        $user = $request->user();

        $review = Review::firstOrCreate([
            'user_id' => $user->id,
            'place_id' => $request->place,
        ], [
            'rating' => $request->rating,
            'comment' => $request->comment,
        ]);

        if (!$review->wasRecentlyCreated) {
            return response()->json([
                'error' => 'A review already exists for this place.',
            ], 409);
        }

        return response($review);
    }

    public function update(Request $request)
    {
        $request->validate(Review::$update_rules);

        /* @var User $user */
        $user = $request->user();

        $review = Review::find($request->review_id);

        if ($review->user_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        if (isset($request->rating)) $review->rating = $request->rating;
        if (isset($request->comment)) $review->comment = $request->comment;

        $review->save();

        return response($review);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'review_id' => 'integer|exists:reviews,id',
        ]);

        /* @var User $user */
        $user = $request->user();

        $review = Review::findOrNew($request->get('review_id'));

        if ($review->user_id !== $user->id) {
            return response(['message' => 'Unauthorized.'], 403);
        }

        $review->forceDelete();

        return response();
    }

}
