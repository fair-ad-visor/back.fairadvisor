<?php

namespace App\Http\Controllers;

use App\Rules\NewUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !\Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Invalid Credentials.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken($request->device_name)->plainTextToken,
        ]);
    }

    public function loginWithGoogle()
    {
        try {
            $googleUser = Socialite::driver('Google')->stateless()->user();
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'google_error' => ['Error trying to login with google account.'],
            ]);
        }

        $user = User::where('email', $googleUser->email)->first();

        if (!$user) {
            throw ValidationException::withMessages([
                'google_error' => ['No user exist for this google account.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken('Login with Google')->plainTextToken,
        ]);
    }

    public function loginWithGithub()
    {
        try {
            $githubUser = Socialite::driver('Github')->stateless()->user();
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'google_error' => ['Error trying to login with github account.'],
            ]);
        }

        $user = User::where('email', $githubUser->email)->first();

        if (!$user) {
            throw ValidationException::withMessages([
                'github_error' => ['No user exist for this github account.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken('Login with Github')->plainTextToken,
        ]);
    }

    public function register(Request $request)
    {
        $request->validate(User::$create_rules);

        $user = User::create([
            'id' => (string)Str::uuid(),
            'username' => $request->username,
            'email' => $request->email,
            'user_type_id' => 1,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => \Hash::make($request->password),
        ]);

        return response()->json([
            'token' => $user->createToken('Sign Up')->plainTextToken,
        ]);
    }

    public function update(Request $request)
    {
        $request->validate(User::$update_rules);

        /* @var User $user */
        $user = $request->user();

        if ($request->exists('username')) $user->username = $request->username;
        if ($request->exists('email')) $user->email = $request->email;
        if ($request->exists('first_name')) $user->first_name = $request->first_name;
        if ($request->exists('last_name')) $user->last_name = $request->last_name;
        if ($request->exists('password')) $user->password = \Hash::make($request->password);

        $user->save();

        return response()->json($user);
    }

    public function revoke_all(Request $request)
    {
        /* @var User $user */
        $user = $request->user();
        $user->tokens->each->delete();
    }
}
