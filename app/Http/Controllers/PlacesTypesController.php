<?php

namespace App\Http\Controllers;

use App\PlaceType;
use Illuminate\Http\Request;

class PlacesTypesController extends Controller
{
    public function fetch()
    {
        return response()->json(PlaceType::all());
    }
}
