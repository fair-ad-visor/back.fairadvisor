<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class IsAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();
        if (!in_array($user->user_type_id, [3])) {
            abort(403, 'Forbidden.');
        }

        return $next($request);
    }
}
