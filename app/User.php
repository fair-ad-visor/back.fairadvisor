<?php

namespace App;

use App\Rules\NewUser;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Airlock\HasApiTokens;

/**
 * App\User
 *
 * @property int $id
 * @property int $user_type_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Airlock\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 * @property-read \App\UserType $user_type
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens, Notifiable;

    public $incrementing = false;

    protected $with = ['user_type'];

    protected $fillable = [
        'id', 'user_type_id', 'email', 'password', 'username', 'first_name', 'last_name',
    ];

    protected $hidden = [
        'id', 'user_type_id', 'password', 'remember_token', 'email_verified_at',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $create_rules = [
        'username' => 'required|string|unique:users,username',
        'email' => 'required|email|unique:users,email',
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'password' => 'required|string',
    ];

    public static $update_rules = [
        'username' => 'string',
        'email' => 'email',
        'first_name' => 'string',
        'last_name' => 'string',
        'password' => 'string',
    ];

    public function tokens()
    {
        return $this->hasMany(UserToken::class, 'user_id', 'id');
    }

    public function user_type()
    {
        return $this->hasOne(UserType::class, 'id', 'user_type_id');
    }
}
