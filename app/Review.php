<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Review
 *
 * @property int $id
 * @property string $user_id
 * @property int $place_id
 * @property int $rating
 * @property string $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review wherePlaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Review whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\User $owner
 * @property-read \App\Place $place
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ReviewComment[] $comments
 * @property-read int|null $comments_count
 */
class Review extends Model
{

    public $table = 'reviews';

    protected $fillable = ['user_id', 'place_id', 'rating', 'comment'];

    protected $hidden = ['user_id'];

    protected $with = ['owner', 'comments'];

    protected $casts = ['rating' => 'integer', 'place_id' => 'integer'];

    protected $appends = ['comments_count'];

    public static $create_rules = [
        'place' => 'required|integer|exists:places,id',
        'rating' => 'required|integer|between:1,5',
        'comment' => 'nullable|string',
    ];

    public static $update_rules = [
        'review_id' => "required|integer|exists:reviews,id",
        'rating' => 'integer|between:1,5',
        'comment' => 'nullable|string',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select(['id', 'first_name', 'last_name']);
    }

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(ReviewComment::class, 'review_id', 'id');
    }

    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

}
