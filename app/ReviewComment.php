<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReviewComment
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $user_id
 * @property int $review_id
 * @property string $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewComment whereUserId($value)
 * @property-read \App\User $owner
 * @property-read \App\Review $review
 * @property-read mixed $edited
 */
class ReviewComment extends Model
{
    protected $table = 'reviews_comments';

    protected $fillable = ['user_id', 'review_id', 'comment'];

    protected $hidden = ['user_id', 'review_id'];

    protected $with = ['owner'];

    protected $appends = ['edited'];

    public static $create_rules = [
        'review' => 'required|integer|exists:reviews,id',
        'comment' => 'nullable|string',
    ];

    public static $update_rules = [
        'review_comment_id' => "required|integer|exists:reviews_comments,id",
        'comment' => 'nullable|string',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select(['id', 'first_name', 'last_name']);
    }

    public function review()
    {
        return $this->belongsTo(Review::class, 'review_id', 'id');
    }

    public function getEditedAttribute()
    {
        return $this->updated_at != $this->created_at;
    }
}
