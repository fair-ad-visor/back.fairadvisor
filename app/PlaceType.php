<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PlaceType
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaceType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaceType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaceType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaceType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PlaceType whereName($value)
 * @mixin \Eloquent
 */
class PlaceType extends Model
{
    public $table = 'places_types';
    public $timestamps = null;

    protected $fillable = ['name'];
}
