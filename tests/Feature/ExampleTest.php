<?php

namespace Tests\Feature;

use App\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        $use_type = UserType::create([
            'name' => 'default'
        ]);

        $response = $this->post('/api/auth/register', [
            'email' => 'nekotiki.perso@gmail.com',
            'username' => 'nekotiki',
            'first_name' => 'Guillaume',
            'last_name' => 'Chateauroux',
            'password' => 'helloworld',
        ]);

        $response->assertStatus(200);

        $response = $this->post('/api/auth/login', [
            'email' => 'nekotiki.perso@gmail.com',
            'password' => 'helloworld',
            'device_name' => 'test',
        ]);

        $token = $response->assertStatus(200)->baseResponse->content();

        $response = $this->get('/api/user/me', [
            'Authorization' => "Bearer $token"
        ]);

        $response->assertStatus(200);
    }
}
