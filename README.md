<p align="center">
    <img src="https://imgur.com/K5Oi6uc.png" height="100">
    <img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400">
</p>

## 🛠 How to setup

First copy the **.env.example** into **.env**

Run

```shell
composer install
```



Then start filling the **.env** files database and oauth services variables

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
GOOGLE_CALLBACK_URL=

GITHUB_KEY=
GITHUB_SECRET=
GITHUB_REDIRECT_URI=
```



When your done with the **.env** file run

```shell
php artisan key:gen
php artisan mig:fresh
php artisan db:seed
```



After that you can start the project with

```shell
php artisan serve
```



## 📜 License

The Laravel framework and this repository are open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
