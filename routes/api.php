<?php

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', 'AuthenticationController@register');
Route::post('/auth/login', 'AuthenticationController@login');
Route::get('/auth/login-with-google', 'AuthenticationController@loginWithGoogle');

Route::get('/login-with-google-redirect', function () {
    return Socialite::with('Google')->redirect();
});

Route::get('/auth/login-with-github', 'AuthenticationController@loginWithGithub');

Route::get('/login-with-github-redirect', function () {
    return Socialite::with('github')->redirect();
});

Route::get('/places', 'PlacesController@fetch');
Route::get('/places/{place}', 'PlacesController@fetchId');

Route::get('/places-types', 'PlacesTypesController@fetch');

Route::get('/review', 'ReviewController@fetch');
Route::get('/review-comment', 'ReviewCommentController@fetch');

Route::middleware(['auth:airlock'])->group(function () {
    Route::get('/user/me', function (Request $request) {
        return $request->user();
    });

    Route::patch('/user', 'AuthenticationController@update');
    Route::post('/user/revoke-all', 'AuthenticationController@revoke_all');

    Route::put('/places', 'PlacesController@create');
    Route::patch('/places', 'PlacesController@update');
    Route::delete('/places', 'PlacesController@delete');

    /// forced to create this route cause of airlock limitation
    /// (airlock need to pass throw the middleware to retrieve the logged in user)
    Route::get('/places/{place}/logged', 'PlacesController@fetchId');

    Route::put('/review', 'ReviewController@create');
    Route::patch('/review', 'ReviewController@update');
    Route::delete('/review', 'ReviewController@delete');

    Route::put('/review-comment', 'ReviewCommentController@create');
    Route::patch('/review-comment', 'ReviewCommentController@update');
    Route::delete('/review-comment', 'ReviewCommentController@delete');
});

// Routes reserved to moderators / administrators
Route::middleware(['auth:airlock', 'is.moderator'])->group(function () {

});

// Routes reserved to administrators
Route::middleware(['auth:airlock', 'is.administrator'])->group(function () {
    Route::get('/dashboard', 'DashboardController@stats');
});
