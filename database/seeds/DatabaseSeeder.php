<?php

use App\Place;
use App\PlaceType;
use App\Review;
use App\User;
use App\UserType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        UserType::firstOrCreate(['id' => 1], ['name' => 'User']);
        UserType::firstOrCreate(['id' => 2], ['name' => 'Moderator']);
        UserType::firstOrCreate(['id' => 3], ['name' => 'Administrator']);

        $user = User::firstOrCreate([
            'username' => 'admin',
            'email' => 'admin@fairadvisor.com',
        ], [
            'id' => (string)Str::uuid(),
            'user_type_id' => 3,
            'first_name' => 'Role',
            'last_name' => 'Administrateur',
            'password' => \Hash::make('test'),
        ]);

        User::firstOrCreate([
            'username' => 'member',
            'email' => 'member@fairadvisor.com',
        ], [
            'id' => (string)Str::uuid(),
            'user_type_id' => 1,
            'first_name' => 'Role',
            'last_name' => 'Member',
            'password' => \Hash::make('test'),
        ]);

        PlaceType::firstOrCreate(['id' => 1], ['name' => 'Hotel']);
        PlaceType::firstOrCreate(['id' => 2], ['name' => 'Restaurant']);

        $lat_lon_list = [
            [45.7456565, 4.8376614],
            [45.746482, 4.838391],
            [45.746572, 4.837231],
            [45.748538, 4.835269],
            [45.749388, 4.838226],
            [45.745687, 4.841441],
            [45.744628, 4.844220],
            [45.744944, 4.841801],
            [45.743336, 4.836704],
            [45.741658, 4.840000],
            [45.742330, 4.840402],
        ];

        foreach ($lat_lon_list as $key => $lat_lon) {
            $place = Place::firstOrCreate(['id' => $key], [
                'owner_id' => $user->id,
                'place_type_id' => 1,
                'name' => "Hotel N°$key",
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin est lectus, consectetur ut dapibus vel, lobortis vel mi. Nulla facilisi. Quisque blandit, justo quis euismod pulvinar, nulla nibh fringilla lectus, ut fringilla ante nibh a lorem. Aliquam bibendum erat at lorem consectetur, id dictum diam egestas.',
                'lat' => $lat_lon[0],
                'lon' => $lat_lon[1],
                'address' => "$key rue des champs",
                'postcode' => "69008",
                'city' => 'Lyon',
                'country' => 'France',
                'site' => 'https://example.com',
                'email' => "hotel-$key@example.com",
                'phone' => '06 99 99 99 99',
            ]);

            Review::create([
                'user_id' => $user->id,
                'place_id' => $place->id,
                'rating' => rand(0, 5),
                'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin est lectus, consectetur ut dapibus vel, lobortis vel mi. Nulla facilisi. Quisque blandit, justo quis euismod pulvinar, nulla nibh fringilla lectus, ut fringilla ante nibh a lorem. Aliquam bibendum erat at lorem consectetur, id dictum diam egestas.',
            ]);
        }
    }
}
